-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jun 03, 2013 at 03:34 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `activitydb`
-- 
CREATE DATABASE `activitydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `activitydb`;

-- --------------------------------------------------------

-- 
-- Table structure for table `actcontenttable`
-- 

CREATE TABLE `actcontenttable` (
  `actID` varchar(20) default NULL COMMENT '活動ID',
  `actName` char(20) default NULL COMMENT '活動名稱',
  `actDesc` text COMMENT '描述',
  `attPlace` text COMMENT '地點',
  `atime` varchar(80) default NULL COMMENT '時間',
  `attKind` char(10) default NULL COMMENT '分類',
  `attComment` text COMMENT '附註',
  `attResult` int(20) default NULL COMMENT '結果'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `actcontenttable`
-- 

INSERT INTO `actcontenttable` VALUES ('1', '高中同學會', '聚餐', '台北市台北火車站美食街', '1-2013/05/31-11-30-am;2-2013/06/01-11-30-am;3-2013/06/01-06-30-pm;5-2013/06/05-1', '聚餐', '已回覆', 0);
INSERT INTO `actcontenttable` VALUES ('2', '家族聚餐', '家族聚餐', '台中市台北火車站美食街', '1-2013/05/31-11-30-am;2-2013/06/01-11-30-am;3-2013/06/11-06-30-pm;4-2013/06/25-1', '聚餐', '待回覆', 0);
INSERT INTO `actcontenttable` VALUES ('3', 'test1', 'xxxx', 'classroom', 'xxxxxx', '2', NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `activitymember`
-- 

CREATE TABLE `activitymember` (
  `memID` int(10) NOT NULL auto_increment,
  `memAcc` text NOT NULL,
  `memPwd` varchar(20) NOT NULL,
  `memMail` text NOT NULL,
  `memPhone` text NOT NULL,
  `memComment` text NOT NULL,
  PRIMARY KEY  (`memID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `activitymember`
-- 

INSERT INTO `activitymember` VALUES (1, 'me', '123456', 'FREDA2009@GMAIL.COM', '00999542', 'NO');

-- --------------------------------------------------------

-- 
-- Table structure for table `acttotaltable`
-- 

CREATE TABLE `acttotaltable` (
  `actID` varchar(10) NOT NULL COMMENT '活動ID',
  `attName` char(20) default NULL COMMENT '參與者',
  `attRole` int(10) NOT NULL default '0' COMMENT '角色',
  `ans` int(20) default NULL COMMENT '回覆',
  `status` int(10) NOT NULL default '0' COMMENT '活動狀態'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `acttotaltable`
-- 

INSERT INTO `acttotaltable` VALUES ('3', 'mary', 1, NULL, 0);
INSERT INTO `acttotaltable` VALUES ('1', '張三', 0, NULL, 0);
INSERT INTO `acttotaltable` VALUES ('2', '李四', 0, NULL, 0);
INSERT INTO `acttotaltable` VALUES ('1', 'me', 1, 10101, 0);
INSERT INTO `acttotaltable` VALUES ('2', 'me', 0, NULL, 0);
INSERT INTO `acttotaltable` VALUES ('3', 'me', 0, NULL, 0);
