/*Tab三畫面之一-新增的團
*/
package com.example.jogroup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class StepNewOne extends Activity {
	EditText actName,actDesc,attPlace;
	Spinner spinner;
	Button btnnext,clear;
	ImageButton btnmap;
	String attkind,loct;
	
	
	private SharedPreferences settings;
    private static final String data = "DATA";
    private static final String nameField = "actName";
    private static final String descField = "actDesc";
    private static final String placeField = "attPlace";
    private static final String kindField ="attKind";
   // private static final String tagField="tag";
	private static final String[] kind={"Eatting","Dating","Meeting","Others"};
    private ArrayAdapter<String> adapter;  

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.new_one_group);
		init();
	}
	
	private void init(){
		actName=(EditText) findViewById(R.id.actName);
		actDesc=(EditText) findViewById(R.id.actDesc);
		attPlace=(EditText) findViewById(R.id.attPlace);
		btnnext=(Button) findViewById(R.id.btnNextStep);
		clear=(Button) findViewById(R.id.clear);
		spinner=(Spinner) findViewById(R.id.spinnner);
		btnmap = (ImageButton) findViewById(R.id.btnmap);
		
		
		adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,kind);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new SpinnerSelectedListener());  
	}
	
	private class SpinnerSelectedListener implements OnItemSelectedListener{
		 @Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
			 
			 attkind=kind[pos].toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub	
			}
	 }
	
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btnNextStep:
				saveDate();
				Intent go =new Intent();
				go.setClass(getBaseContext(), StepNewOneTime.class);
				startActivity(go);
				break;
			case R.id.clear:
				clear();
				break;
				
			case R.id.btnmap:
				loct = attPlace.getText().toString();
				if (loct.length()==0){
					//throw new Exception("loc is Empty");
					Toast.makeText(getBaseContext(), "請輸入地址", Toast.LENGTH_LONG)
					.show();
				} else {

					haveInternet();
				}
				
				break;
		}
	}
	
	private void saveDate(){
		
        settings = getSharedPreferences(data,0);
        settings.edit()
        			.putString(nameField, actName.getText().toString())
        			.putString(descField, actDesc.getText().toString())
        			.putString(placeField, attPlace.getText().toString())
        			.putString(kindField,attkind)
        			//.putString(tagField,"1")
        			.commit();
	}
	
	private void clear(){
		actName.setText("");
		actDesc.setText("");
		attPlace.setText("");
	}
	
	private boolean haveInternet()
    {
    	boolean result = false;
    	ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE); 
    	NetworkInfo info=connManager.getActiveNetworkInfo();
    	if (info == null || !info.isConnected())
    	{
    		Toast.makeText(getBaseContext(), "請檢查網路狀態", Toast.LENGTH_LONG).show();
    		result = false;
    	}
    	else 
    	{
    		if (!info.isAvailable())
    		{
    			Toast.makeText(getBaseContext(), "請檢查網路狀態", Toast.LENGTH_LONG).show();
    			result =false;
    		}
    		else
    		{
    			Intent intent = new Intent();
				intent.setClass(StepNewOne.this, MapViewW.class);
				Bundle bundle = new Bundle();
				bundle.putString("Key_Address", attPlace.getText().toString());
				intent.putExtras(bundle);
				startActivity(intent);
				Toast.makeText(getBaseContext(), loct, Toast.LENGTH_LONG).show();
    			result = true;
    		}
    	}
    	
    	return result;
    }
}