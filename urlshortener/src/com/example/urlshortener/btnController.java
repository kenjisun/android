package com.example.urlshortener;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.connect.AddList;
import com.example.connect.DBUtil;
import com.example.connect.GoogleShortener;
import com.example.connect.insertTask;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class btnController implements OnClickListener, OnItemClickListener{
	
	Activity mother;
	Button shortenBtn;
	Button addBtn_main;
	Button backToMainBtn;
	ImageButton prevBtn, nextBtn, refreshBtn;
	EditText txtName;
	EditText txtURL;
	String[] line1, line2, line3;
	WebView webView;
	EditText webURL;
	ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
	
	public btnController(Activity mother){
		this.mother = mother;
	}
	
	public btnController(Activity mother,String[] line1, String[] line2, String[] line3){
		this.mother = mother;
		this.line1 = line1;
		this.line2 = line2;
		this.line3 = line3;
	}
	
	//buttons event
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0.getTag().equals("addMain")){
			
			mother.setContentView(R.layout.activity_add_url);
			MainActivity.CURRENT_PAGE = 2;
			txtName = (EditText) mother.findViewById(R.id.addName);
			txtURL = (EditText) mother.findViewById(R.id.addURL);
			shortenBtn = (Button) mother.findViewById(R.id.btnShorten);
			shortenBtn.setOnClickListener(this);
			
		}else if(arg0.getTag().equals("btnShorten")){
			
			txtName = (EditText) mother.findViewById(R.id.addName);
			txtURL = (EditText) mother.findViewById(R.id.addURL);
			String inputName = txtName.getText().toString();
			String inputURL = txtURL.getText().toString();
			
			if(URLUtil.isValidUrl(txtURL.getText().toString()) && inputName != null && !inputName.equals("")){
				
				new insertTask(mother,inputName,inputURL).execute();
				
			}else{
				Toast.makeText(mother, "invalid input!", Toast.LENGTH_SHORT).show();
			}
			
		}else if(arg0.getTag().equals("addURL_no")){
			
			mother.setContentView(R.layout.activity_add_url);
			MainActivity.CURRENT_PAGE = 2;
			txtName = (EditText) mother.findViewById(R.id.addName);
			txtURL = (EditText) mother.findViewById(R.id.addURL);
			shortenBtn = (Button) mother.findViewById(R.id.btnShorten);
			shortenBtn.setOnClickListener(this);
			
		}else if(arg0.getTag().equals("refreshButton")){
			webView.loadUrl( "javascript:window.location.reload( true )" );
		}else if(arg0.getTag().equals("prevButton")){
			if(webView.canGoBack()){
		        webView.goBack();
			}
		}else if(arg0.getTag().equals("nextButton")){
			if(webView.canGoForward()){
		        webView.goForward();
			}
		}else if(arg0.getTag().equals("backButton")){
			result = new DBUtil().loadURL(mother);
			new MainActivity().initMainView(mother, result);
		}
	}

	//list view event
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
		// TODO Auto-generated method stub
		//check internet connection first
		ConnectivityManager cm = (ConnectivityManager) mother.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
			if (ni == null) {
			// There are no active networks.
				Toast.makeText(mother, "Please check your internet connection", Toast.LENGTH_SHORT).show();
			} else {
				mother.setContentView(R.layout.webview_layout);
				webView = (WebView) mother.findViewById(R.id.webView);
				webURL = (EditText) mother.findViewById(R.id.urlField);
				prevBtn = (ImageButton) mother.findViewById(R.id.prevButton);
				nextBtn = (ImageButton) mother.findViewById(R.id.nextButton);
				refreshBtn = (ImageButton) mother.findViewById(R.id.refreshButton);
				backToMainBtn = (Button) mother.findViewById(R.id.backButton);
				
				webView.setWebViewClient(new MyWebViewClient());
				WebSettings webSettings = webView.getSettings();
				webSettings.setBuiltInZoomControls(true);
				webSettings.setJavaScriptEnabled(true);
				prevBtn.setOnClickListener(this);
				nextBtn.setOnClickListener(this);
				refreshBtn.setOnClickListener(this);
				backToMainBtn.setOnClickListener(this);
				
                webView.loadUrl(line2[position]);
                webURL.setText(line3[position]);
                
			}
		//Toast.makeText(mother, "=="+line1[position]+"=="+line2[position]+"=="+line3[position], Toast.LENGTH_SHORT).show();
		
	}
	
	
	 private class MyWebViewClient extends WebViewClient {
	        @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
	            view.loadUrl(url);
	            return true;
	        }
	    }
	
}
