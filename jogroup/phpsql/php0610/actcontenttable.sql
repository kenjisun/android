-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 06 月 10 日 05:50
-- 服务器版本: 5.5.24-log
-- PHP 版本: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `activitydb`
--

-- --------------------------------------------------------

--
-- 表的结构 `actcontenttable`
--

CREATE TABLE IF NOT EXISTS `actcontenttable` (
  `actid` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `actname` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `actdesc` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attplace` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `atime` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attkind` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attcomment` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `attresult` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `actcontenttable`
--

INSERT INTO `actcontenttable` (`actid`, `actname`, `actdesc`, `attplace`, `atime`, `attkind`, `attcomment`, `attresult`) VALUES
('4b4777a9-94fc-4028-a', 'testact', 'food', 'taipei main station', '4-Jun-2013 10:45,10-Jun-2013 11:45,1-Jul-2013 12:45,1-Jul-2013 13:45,3-Jul-2013 ', 'Eatting', '0', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
