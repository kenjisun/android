/* 登入畫面-第一個畫面 */
package com.example.jogroup;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.library.DatabaseHandler;
import com.example.library.UserFunctions;

import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class StepLogin extends Activity {

	EditText account,password;
	Button login,register,abouto2party;
	CheckBox checkBox;
	TextView loginErrorMsg;
	
	private SharedPreferences settings;
    private static final String data = "DATA";
    
	// JSON Response node names
	private static String KEY_SUCCESS = "success";
	private static String KEY_ERROR = "error";
	private static String KEY_ERROR_MSG = "error_msg";
	private static String KEY_UID = "uid";
	private static String KEY_NAME = "name";
	private static String KEY_ACCOUNT = "account";
	private static String KEY_CREATED_AT = "created_at";
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.login);
		
		StrictMode.ThreadPolicy policy =new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		account = (EditText) findViewById(R.id.account);
		password = (EditText) findViewById(R.id.password);
		password.setTypeface(Typeface.DEFAULT);
		login = (Button) findViewById(R.id.login);
		register = (Button) findViewById(R.id.register);
		abouto2party = (Button) findViewById(R.id.abouto2party);
		loginErrorMsg = (TextView) findViewById(R.id.loginErrorMsg);
//		checkBox = (CheckBox) findViewById(R.id.checkBox1);
		
		
//		SharedPreferences getPre = getSharedPreferences("loginInfo", 0);
//		String acc = getPre.getString("account", "");
//		String pass = getPre.getString("password", "");
//		String checked = getPre.getString("isChecked", "");
//		account.setText(acc);
//		password.setText(pass);
//		if(checked.equals("1")){
//			checkBox.setChecked(true);
//		}
		
		
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    settings = getSharedPreferences(data,0);

				String acc = account.getText().toString();
				String pass = password.getText().toString();
				UserFunctions userFunction = new UserFunctions();
				Log.d("Button", "Login");
				JSONObject json = userFunction.loginUser(acc, pass);
				
				// check for login response
				try {
					if (json.getString(KEY_SUCCESS) != null) {
						loginErrorMsg.setText("");
						String res = json.getString(KEY_SUCCESS); 
						if(Integer.parseInt(res) == 1){
							// user successfully logged in
							// Store user details in SQLite Database
							DatabaseHandler db = new DatabaseHandler(getApplicationContext());
							JSONObject json_user = json.getJSONObject("user");
							
							// Clear all previous data in database
							userFunction.logoutUser(getApplicationContext());
							db.addUser(json_user.getString(KEY_NAME), json_user.getString(KEY_ACCOUNT), json.getString(KEY_UID), json_user.getString(KEY_CREATED_AT));						
							
							// Launch Dashboard Screen
							Intent dashboard = new Intent(getApplicationContext(), CustomTab.class);
							
							// Close all views before launching Dashboard
							dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(dashboard);
							
							// Close Login Screen
							finish();
//							Toast.makeText(getBaseContext(), acc, 1).show();
							settings.edit().putString("name",acc).commit();

						}else{
							// Error in login
							loginErrorMsg.setText("Incorrect username/password");
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
//					Intent it = new Intent();
//					it.setClass(StepLogin.this,CustomTab.class);
//					StepLogin.this.finish();
//					startActivity(it);


			}
		});
		
		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent it = new Intent();
				it.setClass(StepLogin.this,RegisterActivity.class);
				StepLogin.this.finish();
				startActivity(it);
			}
		});
		
		abouto2party.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent it = new Intent();
				it.setClass(StepLogin.this,AboutO2Party.class);
				startActivity(it);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("確定要離開嗎？")
				.setPositiveButton("確定", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				})
				.setNegativeButton("取消", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				}).show();
		return;
	}

}
