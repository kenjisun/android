/*tab選項*/
package com.example.jogroup;

public class Constant {

	
	public static final class ConValue{
		
		public static int mImageViewArray[] = {R.drawable.tab1,
											R.drawable.tab2,
											R.drawable.tab3};

		public static String mTextviewArray[] = {"我開的團", "邀請訊息", "新增一團"};
		
		
		public static Class mTabClassArray[]= {StepCreate.class,
												StepInvitationList.class,
												StepNewOne.class};
	}
}
