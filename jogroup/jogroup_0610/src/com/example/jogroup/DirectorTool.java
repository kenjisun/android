/*我開的團2-團長工具*/
package com.example.jogroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.phpsql.CustomHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;


public class DirectorTool extends Activity {
	//////////init///////////
	Button btnalarm;
	TextView kind,actname,deadline,count,desc,place,friendList;
	Button btn_map;
	String actid;
	String[] times;
	ListView lv;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.directortool);
		kind=(TextView) findViewById(R.id.tv_kind);
		actname=(TextView) findViewById(R.id.tv_actname);
		count=(TextView) findViewById(R.id.tv_count);
		desc=(TextView) findViewById(R.id.tv_desc);
		place=(TextView) findViewById(R.id.tv_place);
		lv=(ListView) findViewById(R.id.lv);
		friendList = (TextView) findViewById(R.id.friendList);
		init();
		
		
		
		btnalarm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DirectorTool.this,AlarmClock.class);
				startActivity(intent);
			}
		});
	}
	
	private void init()
	{
		btnalarm = (Button) findViewById(R.id.SetAlarm);
		
		for(JSONObject json_data:StepCreate.json_data_List){
			
			Log.d("fy2",json_data.toString());
			
			
			
			///////////setText for filed/////////
			
			
			try {
				actid = json_data.getString("actid");
				Log.d("fyy",actid);

				kind.setText(json_data.getString("attkind"));
				Log.d("fy3",json_data.getString("attkind"));

				actname.setText(json_data.getString("actname"));
				Log.d("fy4",json_data.getString("actname"));
				
				count.setText(json_data.getString("ans_count"));
				Log.d("fy5",json_data.getString("ans_count"));

				desc.setText(json_data.getString("actdesc"));
				Log.d("fy6",json_data.getString("actdesc"));

				place.setText(json_data.getString("attplace"));
				Log.d("fy7",json_data.getString("attplace"));
				
				Log.d("fy8",json_data.getString("atime"));
				times=json_data.getString("atime").split(",");
				String tmp = "";
				List<String> timeData = new ArrayList<String>();
				List<String> timeDataLength = new ArrayList<String>();
				
				for(int i=0; i<times.length; i++) {
					tmp += times[i] + " ; ";
					timeDataLength.add(times[i]);
					if(/*!times[i].equals("") && times[i]!=null && times[i].equals(" ")*/timeDataLength.get(i).length()>1){
						timeData.add(times[i]);
						Log.d("fy89",timeData.get(i) + ", len=" + timeData.get(i).length());
					}
				}
				Log.d("fy9",tmp);
				
				String a;
//				ArrayAdapter<String> adapter =new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,times);
				ArrayAdapter<String> adapter =new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,timeData);
				lv.setAdapter(adapter);
				
				
				

				
				
			} 
			
			catch (JSONException e) {
				Log.e("fy100",e.getLocalizedMessage());

			}
			
			
			
		}
		
		
		read_HttpGet(actid);
		
	}
	
	
	
	private void read_HttpGet(String actid) {

		String get_response = null;

		// call executeHttpPost method passing necessary parameters
		try {
			get_response = CustomHttpClient
					.executeHttpGet("http://192.168.0.32/android_connect/get_invited_list.php?actid="
							+ actid);
			
			// store the result returned by PHP script that runs MySQL query
			String get_result = get_response;

			Log.i("get_new_sam", get_result + "");
			try {
				Log.i("get_new_in", " in try");
				JSONArray jobject = new JSONArray(get_result);
				
				Log.i("get_new_inin", get_result + "");
				
				
				
			////// 解開JSONArray
				String nameKing="";
				ArrayList<String> names = new ArrayList<String>();
				//wantToSplit.add(jobject.getString(0).split(","));
				for(int i =0;i<jobject.length();i++){
					names.add(jobject.getString(i));
					nameKing += names.get(i)+"-";
				}
				Log.i("get_new_inin2", names.get(0)+"===="+names.get(1));
				//JSONArray jArray =(JSONArray) jobject.getJSONArray(0);
				//Log.d("jarry",jArray+"");
				
				friendList.setText(nameKing);
//				for(int i=0;i<jArray.length();i++){
//					
//					Log.d("jarry",jArray.get(i).toString());
//					
//				}
				
				
				
				
				/*JSONArray jArray = (JSONArray) jobject.getJSONArray("product").get(0);
				Log.d("fy",jArray.toString());
				Log.d("fy",jArray.get(0).toString());
				Log.d("fy",jArray.get(1).toString());
				Log.d("fy",jArray.get(2).toString());
				Log.d("fy",jArray.get(3).toString());
				Log.d("fy",jArray.get(4).toString());
				*/

			} catch (JSONException e) {
				Log.e("log_tag2", "get_Error parsing data " + e.toString());
			}

		}

		catch (Exception e) {
			Log.e("log_tag3", "get_Error in http connection!!" + e.toString());
		}

	}
	
	private boolean haveInternet()
    {
    	boolean result = false;
    	ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE); 
    	NetworkInfo info=connManager.getActiveNetworkInfo();
    	if (info == null || !info.isConnected())
    	{
    		Toast.makeText(getBaseContext(), "請檢查網路狀態", Toast.LENGTH_LONG).show();
    		result = false;
    	}
    	else 
    	{
    		if (!info.isAvailable())
    		{
    			Toast.makeText(getBaseContext(), "請檢查網路狀態", Toast.LENGTH_LONG).show();
    			result =false;
    		}
    		else
    		{
    			Intent intent = new Intent();
				intent.setClass(DirectorTool.this, MapViewW.class);
				Bundle bundle = new Bundle();
				bundle.putString("Key_Address", place.getText().toString());
				intent.putExtras(bundle);
				startActivity(intent);
				Toast.makeText(getBaseContext(), place.getText().toString(), Toast.LENGTH_LONG).show();
    			result = true;
    		}
    	}
    	
    	return result;
    }
}