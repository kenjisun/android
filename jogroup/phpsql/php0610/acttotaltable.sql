-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 06 月 10 日 05:50
-- 服务器版本: 5.5.24-log
-- PHP 版本: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `activitydb`
--

-- --------------------------------------------------------

--
-- 表的结构 `acttotaltable`
--

CREATE TABLE IF NOT EXISTS `acttotaltable` (
  `actid` varchar(20) CHARACTER SET latin1 NOT NULL,
  `attname` varchar(15) CHARACTER SET latin1 NOT NULL,
  `attrole` int(11) NOT NULL,
  `ans` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `acttotaltable`
--

INSERT INTO `acttotaltable` (`actid`, `attname`, `attrole`, `ans`, `status`) VALUES
('c62b1be6-5a5a-4b1b-9', 'knox', 1, 0, 0),
('c62b1be6-5a5a-4b1b-9', 'goo', 0, 0, 0),
('c62b1be6-5a5a-4b1b-9', 'sam', 0, 0, 0),
('2a666569-98da-4871-9', 'ma', 1, 0, 0),
('2a666569-98da-4871-9', 'robert', 0, 0, 0),
('2a666569-98da-4871-9', 'mary', 0, 0, 0),
('2a666569-98da-4871-9', 'john', 0, 0, 0),
('3f6bcaef-6bf3-44fd-a', 'ma', 1, 0, 0),
('3f6bcaef-6bf3-44fd-a', 'john', 0, 0, 0),
('1ca4e554-cfb3-4e91-9', 'ma', 1, 0, 0),
('1ca4e554-cfb3-4e91-9', 'robert', 0, 0, 0),
('1ca4e554-cfb3-4e91-9', 'john', 0, 0, 0),
('1ca4e554-cfb3-4e91-9', 'kenji', 0, 0, 0),
('4b4777a9-94fc-4028-a', 'robert', 1, 0, 0),
('4b4777a9-94fc-4028-a', 'mary', 0, 0, 0),
('4b4777a9-94fc-4028-a', 'john', 0, 0, 0),
('4b4777a9-94fc-4028-a', 'goo', 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
