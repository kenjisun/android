package com.example.androidhive;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class PhpSql extends Activity{
	
	Button btnViewProducts;
	Button btnNewProduct;
	private static final String TAG_ATTNAME = "attname";
	//public String attname="aa";

	// Progress Dialog
	private ProgressDialog pDialog;

	JSONParser jsonParser = new JSONParser();
	String actID="bb",attName="xxx----";
	int attRole=1,ans=0,status=1;
	
	String actname="party ";
	String actdesc="中 ";
	String attplace="taipei ";
	String atime="2010 ";
	String attkind="111 ";
	String attcomment="yyyy ";
	String attresult="2";
	
	String query_actID,query_attName;
	int query_attRole,query_ans,query_status;
	String query_actname,query_actdesc,query_attplace,query_atime,query_attkind,query_attcomment;
	int query_attresult;
	
	
	//------------------------------------------------
	//EditText txtName;
	//EditText txtPrice;
	//EditText txtCreatedAt;
	Button btnSave;
	Button btnDelete;

	//String attname;

	// Progress Dialog
//	private ProgressDialog pDialog;

	// JSON parser class
	//JSONParser jsonParser = new JSONParser();

	// single product url
	private static final String url_product_detials = "http://10.0.2.2/android_connect/get_product_details.php";

	// url to update product
	private static final String url_update_product = "http://10.0.2.2/android_connect/update_product.php";
	
	// url to delete product
	private static final String url_delete_product = "http://10.0.2.2/android_connect/delete_product.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCT = "product";
	private static final String TAG_ACTID = "actid";
//	private static final String TAG_ATTNAME = "attname";
	private static final String TAG_ATTROLE = "attrole";
	private static final String TAG_ANS = "ans";
	private static final String TAG_STATUS = "status";
	private static final String TAG_ACTNAME="actname";
	private static final String TAG_ACTDESC="actdesc";
	private static final String TAG_ATTPLACE="attplace";
	private static final String TAG_ATIME="atime";
	private static final String TAG_ATTKIND="attkind";
	private static final String TAG_ATTCOMMENT="attcomment";
	private static final String TAG_ATTRESULT="attresult";
	EditText et1;
	String query=TAG_ACTID;

	
	
	
	//--------------------------------------------------
	// url to create new product
	private static String url_create_product = "http://10.0.2.2/android_connect/create_product.php";

	// JSON Node names
	//private static final String TAG_SUCCESS = "success";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_screen);
		UpdateDB update=new UpdateDB("bb", "dd", 22);
		update.updateDB();
		
		// Buttons
		btnViewProducts = (Button) findViewById(R.id.btnViewProducts);
		btnNewProduct = (Button) findViewById(R.id.btnCreateProduct);
		et1=(EditText)findViewById(R.id.et1);
		
		// view products click event
		btnViewProducts.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// Launching All products Activity
				//Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
				//Intent i = new Intent(getApplicationContext(), EditProductActivity.class);
				//i.putExtra(TAG_ATTNAME, attname);
				//startActivity(i);
				new GetProductDetails().execute(et1.getText().toString());
				
			}
		});
		
		// view products click event
		btnNewProduct.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// Launching create new product activity
				//Intent i = new Intent(getApplicationContext(), NewProductActivity.class);
				//startActivity(i);
				new CreateNewProduct().execute("one");
				
			}
		});
	}
	

	/**
	 * Background Async Task to Create new product---開團
	 * */
	class CreateNewProduct extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(PhpSql.this);
			pDialog.setMessage("Creating Product..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Creating product
		 * */
		protected String doInBackground(String... args) {
			String id = actID;
			String att_name = attName;
			String role=String.valueOf(attRole);
			String feedback=String.valueOf(ans);
			String att_status=String.valueOf(status);
			String act_name=actname;
			String act_desc=actdesc;
			String att_place=attplace;
			String a_time=atime;
			String att_kind=attkind;
			String att_comment=attcomment;
			String att_result=attresult;
			

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("actid", id));
			params.add(new BasicNameValuePair("attname", att_name));
			params.add(new BasicNameValuePair("attrole", role));
			params.add(new BasicNameValuePair("ans", feedback));
			params.add(new BasicNameValuePair("status", att_status));
			//-------------------------------------------------------
			//params.add(new BasicNameValuePair("actid", id));
			if(args[0]=="one"){
			params.add(new BasicNameValuePair("actname", act_name));
			params.add(new BasicNameValuePair("actdesc", act_desc));
			params.add(new BasicNameValuePair("attplace", att_place));
			params.add(new BasicNameValuePair("atime", a_time));
			params.add(new BasicNameValuePair("attkind", att_kind));
			params.add(new BasicNameValuePair("attcomment", att_comment));
			params.add(new BasicNameValuePair("attresult", att_result));
			}
			
			// getting JSON Object
			// Note that create product url accepts POST method
			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"POST", params);
			
			// check log cat fro response
			Log.d("Create Response", json.toString());

			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// successfully created product
				//	Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
					//startActivity(i);
					
					// closing this screen
					//finish();
				} else {
					System.out.println("failed to creat xxxxxx");
					// failed to create product
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once done
			pDialog.dismiss();
		}

	}
	



	/**
	 * Background Async Task to Get complete product details---查詢
	 * */
	class GetProductDetails extends AsyncTask<String, String, JSONArray> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		//@Override
		//protected void onPreExecute() {
			//super.onPreExecute();
			//pDialog = new ProgressDialog(PhpSql.this);
		//	pDialog.setMessage("Loading product details. Please wait...");
			//pDialog.setIndeterminate(false);
		//	pDialog.setCancelable(true);
			//pDialog.show();
		//}

		/**
		 * Getting product details in background thread
		 * */
		protected JSONArray doInBackground(String... arg) {

			// updating UI from Background Thread
			//runOnUiThread(new Runnable() {
				//public void run() {
					// Check for success tag
					int success;
					JSONArray productObj;
					try {
						// Building Parameters
						String query1=query;
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair(query1, arg[0]));
						System.out.println(params);
						// getting product details by making HTTP request
						// Note that product details url will use GET request
						JSONObject json = jsonParser.makeHttpRequest(
								url_product_detials, "GET", params);
						System.out.println(json);

						// check your log for json response
						Log.d("Single Product Details", json.toString());
						
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						System.out.println(success);
						if (success == 1) {
							// successfully received product details
							productObj = json
									.getJSONArray(TAG_PRODUCT); // JSON Array
							
							// get first product object from JSON Array
							//JSONObject product = productObj.getJSONObject(0);

							// product with this pid found
							// Edit Text
						//	txtName = (EditText) findViewById(R.id.inputName);
						//	txtPrice = (EditText) findViewById(R.id.inputPrice);
						//	txtDesc = (EditText) findViewById(R.id.inputDesc);

							// display product data in EditText
							//txtDesc.setText(product.getString(TAG_ACTID));
							//txtName.setText(product.getString(TAG_ATTNAME));
							//txtPrice.setText(product.getString(TAG_ATTROLE));
							//txtDesc.setText(product.getString(TAG_ANS));
							//txtDesc.setText(product.getString(TAG_STATUS));
							return productObj;

						}//else{
							// product with pid not found
						//}
					} catch (JSONException e) {
						e.printStackTrace();
					}
			//	}
	//		});

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(JSONArray result) {
			// dismiss the dialog once got all details
			try{
				
				
				if(result!=null){
					
				JSONObject product = result.getJSONObject(0);
				if(query==TAG_ATTNAME){
				query_actID=product.getString(TAG_ACTID);
				query_attName=product.getString(TAG_ATTNAME);
				query_attRole=Integer.parseInt(product.getString(TAG_ATTROLE));
				query_ans=Integer.parseInt(product.getString(TAG_ANS));
				query_status=Integer.parseInt(product.getString(TAG_STATUS));
				System.out.println("actid: "+query_actID);
				System.out.println("attname: "+query_attName);
				System.out.println("attrole: "+query_attRole);
				System.out.println("Answer: "+query_ans);
				System.out.println("status: "+query_status);
				}
				
				else if(query==TAG_ACTID){
					
					query_actID=product.getString(TAG_ACTID);
					query_actname=product.getString(TAG_ACTNAME);
					query_actdesc=product.getString(TAG_ACTDESC);
					query_attplace=product.getString(TAG_ATTPLACE);
					query_atime=product.getString(TAG_ATIME);
					query_attkind=product.getString(TAG_ATTKIND);
					query_attcomment=product.getString(TAG_ATTCOMMENT);
					query_attresult=Integer.parseInt(product.getString(TAG_ATTRESULT));
					System.out.println("actid: "+query_actID);
					System.out.println("actname: "+query_actname);
					System.out.println("attrole: "+query_actdesc);
					System.out.println("actdesc: "+query_attplace);
					System.out.println("atime: "+query_atime);
					System.out.println("attkind: "+query_attkind);
					System.out.println("attcomment: "+query_attcomment);
					System.out.println("attresult: "+query_attresult);
				}
				}
			}catch(JSONException e) {
						e.printStackTrace();
			}
			//pDialog.dismiss();
		}
	}

}
