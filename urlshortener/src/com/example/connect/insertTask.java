package com.example.connect;

import java.util.ArrayList;

import com.example.urlshortener.CustomArrayAdapter;
import com.example.urlshortener.MainActivity;
import com.example.urlshortener.R;
import com.example.urlshortener.btnController;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class insertTask extends AsyncTask<String, String, String> {

	private ProgressDialog pDialog;
	Activity mother;
	String inputName, inputURL;
	String[] names, shortURLs, longURLs;
	private int successOrNot = 0;
	Button addBtn_main;
	TextView txtRowCount;
	ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
	
	public insertTask(Activity mother, String inputName,String inputURL){
		this.mother = mother;
		this.inputName = inputName;
		this.inputURL = inputURL;
	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(mother);
		pDialog.setMessage("shortening the URL...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
		new GoogleShortener(inputURL).execute();
	}
	
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		if(new DBUtil().insertURL(mother, inputName, inputURL)){
			successOrNot = 1;
		}
		
		return null;
	}

	protected void onPostExecute(String file_url) {
		// dismiss the dialog once done
		if(pDialog.isShowing()){
			pDialog.dismiss();
		}
		
		if(successOrNot == 1){
			Toast.makeText(mother, "Add success...", Toast.LENGTH_SHORT).show();
			//load data from device's storage
			result = new DBUtil().loadURL(mother);
			new MainActivity().initMainView(mother,result);
			
		}else{
			Toast.makeText(mother, "error occured!", Toast.LENGTH_SHORT).show();
		}
	}

}
