/*Tab三畫面之二-收到邀請*/
package com.example.jogroup;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;

public class StepInvitationList extends Activity {
	ListView lv2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_invitation_list);

		init();
		showlist2();
		
		lv2.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long i) {
				Intent intent = new Intent(StepInvitationList.this,StepInvitation.class);
				startActivity(intent);
			}
		});
	}

	private void init() {
		lv2 = (ListView) findViewById(R.id.MyInvitationList);
	}

	public void showlist2() {
		Log.d("listview", "showlist2");
		ArrayList<HashMap<String, Object>> outputArray = new ArrayList<HashMap<String, Object>>();

		for (int i = 0; i < category.length; i++) {
			HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("category", category[i]);
			item.put("other", other[i]);
			outputArray.add(item);
		}
		SimpleAdapter myAdapter2 = new SimpleAdapter(this, outputArray,
				R.layout.row_invitation, new String[] { "category", "other" },
				new int[] { R.id.textcategory2, R.id.textother2 });

		lv2.setAdapter(myAdapter2);
	}

	private static String[] category = { "(聚餐)", "(開會)", "(出遊)", "(其他)" };
	private static String[] other = { "離職party", "和巨匠開會", "出國去玩", "其他" };
}