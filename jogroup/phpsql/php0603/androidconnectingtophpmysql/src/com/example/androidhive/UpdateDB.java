package com.example.androidhive;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.androidhive.EditProductActivity.SaveProductDetails;

public class UpdateDB {
	String attname, actid;
	int ans;
	int update_result;
	// String type;
	// String update_content;

	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	// url to update product
	private static final String url_update_product = "http://10.0.2.2/android_connect/update_product.php";

	// url to delete product
	private static final String url_delete_product = "http://10.0.2.2/android_connect/delete_product.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCT = "product";
	private static final String TAG_ACTID = "actid";
	private static final String TAG_ATTNAME = "attname";
	private static final String TAG_ATTROLE = "attrole";
	private static final String TAG_ANS = "ans";
	private static final String TAG_STATUS = "status";
	private static Handler handler;

	public UpdateDB(String id, String name, int answer) {
		actid = id;
		attname = name;
		ans = answer;
	}

	public void updateDB(Handler h) {
		// starting background task to update product
		handler=h;
		new SaveProductDetails().execute();
		/*
		Log.e("hhhh", "------------------------" + update_result);
		handler=new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				Log.e("hhhhiii", "------------------------" + update_result);
				System.out.println("XXXXXXX"+update_result);
			}};
			*/
		
		 
		 
	}

	/**
	 * Background Async Task to Save product Details
	 * */
	class SaveProductDetails extends AsyncTask<String, Void, Integer> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */

		/**
		 * Saving product
		 * 
		 * @return
		 * */
		protected Integer doInBackground(String... args) {

			// getting updated data from EditTexts

			
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_ACTID, actid));
			params.add(new BasicNameValuePair(TAG_ATTNAME, attname));
			// params.add(new BasicNameValuePair(TAG_ATTROLE, role));
			params.add(new BasicNameValuePair(TAG_ANS, String.valueOf(ans)));

			// sending modified data through http request
			// Notice that update product url accepts POST method
			JSONObject json = jsonParser.makeHttpRequest(url_update_product,
					"GET", params);
			int success = 3;
			int result=0;
			// check json success tag
			 ;
			try {
				result = json.getInt(TAG_SUCCESS);
				
				// Log.e("gggggggggggg", "------------------------" + update_result)

				if (result == 1) {
					// successfully updated
					success = result;
				} else {
					success = 0;// failed to update product
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// return success;
			Log.e("bbbbbbbbbbb", "------------------------" + result+"qqqqqqqqqqqq");
			update_result=success;
			
			 Message message;
			  int obj = success;
			  message = handler.obtainMessage(1,obj);
			  handler.sendMessage(message);
			// handler.sendEmptyMessage(0);
			
			return success;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
			// dismiss the dialog once product uupdated
			
			// System.out.println("gggggggggggg"+update_result);

		}
	}
}
