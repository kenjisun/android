/*tab頁面,参考Constant.java*/
package com.example.jogroup;

import com.example.jogroup.Constant.ConValue;
import com.example.jogroup.R.color;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;

public class CustomTab extends TabActivity{

//	private final static String TAG = "CustomTab";
	
	private TabHost		m_tabHost;
	
	private LayoutInflater mLayoutInflater;
	
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.main_tab_layout1);
        
        init();
    }
	  
	private void init()
	{
		m_tabHost = getTabHost();
		mLayoutInflater = LayoutInflater.from(this);
		
		int count = ConValue.mTabClassArray.length;		
		for(int i = 0; i < count; i++)
		{	
			TabSpec tabSpec = m_tabHost.newTabSpec(ConValue.mTextviewArray[i]).
													setIndicator(getTabItemView(i)).
													setContent(getTabItemIntent(i));
			m_tabHost.addTab(tabSpec);
			m_tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.selector_tab_background);
		}
        
	}
	
	private View getTabItemView(int index)
	{

		View view = mLayoutInflater.inflate(R.layout.tab_item_view, null);
	
		ImageView imageView = (ImageView) view.findViewById(R.id.imageview);

		if (imageView != null)
		{
			imageView.setImageResource(ConValue.mImageViewArray[index]);
			
		}
		
		TextView textView = (TextView) view.findViewById(R.id.textview);
		textView.setTextColor(color.purple);
		textView.setText(ConValue.mTextviewArray[index]);
	
		return view;
	}
	
	private Intent getTabItemIntent(int index)
	{
		Intent intent = new Intent(this, ConValue.mTabClassArray[index]);
		
		return intent;
	}
	
	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("確定要離開嗎？")
				.setPositiveButton("確定", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				})
				.setNegativeButton("取消", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				}).show();
		return;
	}
}
