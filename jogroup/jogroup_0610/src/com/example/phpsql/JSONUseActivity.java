package com.example.phpsql;
import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import com.example.phpsql.CustomHttpClient;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class JSONUseActivity extends Activity {
 
 ListView lv;
 ArrayList<String> data=new ArrayList<String>() ;
 ArrayAdapter <String>adapter;

 
 String returnString;   // to store the result of MySQL query after decoding JSON
 
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
   
     super.onCreate(savedInstanceState);
       
     
     StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
     .detectDiskReads().detectDiskWrites().detectNetwork() // StrictMode is most commonly used to catch accidental disk or network access on the application's main thread
     .penaltyLog().build());
      
    // setContentView(R.layout.activity_jsonuse);
   // lv=(ListView) findViewById(R.id.lv);
     
     lv.post(new Runnable(){

		@Override
		public void run() {
			// TODO Auto-generated method stub
			visitExternalLinks();
		}});
     
                
    } 

    
    private void visitExternalLinks(){
    	
    	 String response = null;
         
         // call executeHttpPost method passing necessary parameters 
         try {
    response = CustomHttpClient.executeHttpPost("http://10.0.2.2/android_connect_db.php");
    
    // store the result returned by PHP script that runs MySQL query
    String result = response.toString();
    
    Log.i("sam2",result+"");
             
     //parse json data
        try{
       	 
          returnString = "";
          JSONArray jArray = new JSONArray(result);
          
          Log.i("sam3","this is a "+jArray);
          
                for(int i=0;i<jArray.length();i++){
               	 
                        JSONObject json_data = jArray.getJSONObject(i);
                        Log.i("sam4","loop : "+i);
                        
                        data.add(json_data.getString("attPlace"));
                        
                        Log.i("sam5",data+"");
                      
                     adapter=new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,data);
                        
                     lv.setAdapter(adapter);
                     
               }
        }
        
        catch(JSONException e){
                Log.e("log_tag", "Error parsing data "+e.toString());
        }        
                
        }
    	
     
       catch (Exception e) {
             Log.e("log_tag","Error in http connection!!" + e.toString());     
            }
     
    }  
        
}
