package com.example.androidhive;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;

public class GetResult {

	private static final String url_get_result = "http://10.0.2.2/android_connect/get_result.php";
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCT = "product";
	private static final String TAG_ACTID = "actid";
	private static final String TAG_ANS = "ans";
	//private static android.os.Handler handler;
	ArrayList<String> list;
	Activity myact;

	String query = TAG_ACTID;
	JSONParser jsonParser = new JSONParser();
	public GetResult(Activity act){
		myact=act;
	}

	public void getresult(String actid) {

		new GetAns(myact).execute(actid);
	}

	class GetAns extends AsyncTask<String, Void, JSONArray> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		private Activity activity;
		 //private ProgressDialog dialog;
		 private AsyncTaskListener callback;

		 public GetAns(Activity act) {
		  this.activity = act;
		  this.callback = (AsyncTaskListener)act;
		 }

		/**
		 * Getting product details in background thread
		 * */
		protected JSONArray doInBackground(String... arg) {

			// updating UI from Background Thread
			// runOnUiThread(new Runnable() {
			// public void run() {
			// Check for success tag
			int success;
			JSONArray productObj;
			try {
				// Building Parameters
				String query1 = query;
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair(query1, arg[0]));
				System.out.println(params);
				// getting product details by making HTTP request
				// Note that product details url will use GET request
				JSONObject json = jsonParser.makeHttpRequest(url_get_result,
						"GET", params);
				System.out.println(json);

				// check your log for json response
				Log.d("Single Product Details", json.toString());

				// json success tag
				success = json.getInt(TAG_SUCCESS);
				System.out.println(success);
				if (success == 1) {
					// successfully received product details
					System.out.println("lllllllllllllllllll");
					productObj = json.getJSONArray(TAG_PRODUCT); // JSON Array

					// get first product object from JSON Array
					// JSONObject product = productObj.getJSONObject(0);

					// product with this pid found
					// Edit Text
					// txtName = (EditText) findViewById(R.id.inputName);
					// txtPrice = (EditText) findViewById(R.id.inputPrice);
					// txtDesc = (EditText) findViewById(R.id.inputDesc);

					// display product data in EditText
					// txtDesc.setText(product.getString(TAG_ACTID));
					// txtName.setText(product.getString(TAG_ATTNAME));
					// txtPrice.setText(product.getString(TAG_ATTROLE));
					// txtDesc.setText(product.getString(TAG_ANS));
					// txtDesc.setText(product.getString(TAG_STATUS));
					System.out.println("asasasasasasasFFFFFFF");
					/*
					Message message;
					JSONArray obj=productObj;
					//String obj="OK";
					 message = handler.obtainMessage(1,obj);
					 handler.sendMessage(message);
					 */
					return productObj;
					

				}// else{
					// product with pid not found
				// }
			} catch (JSONException e) {

				e.printStackTrace();
			}
			// }
			// });

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(JSONArray result) {
			// dismiss the dialog once got all details

			try {

				if (result != null) {
					System.out.println("FFFFFFFFFFFFFFFFFFFFFFFFFF");
					JSONArray product = result.getJSONArray(0);
					System.out.println("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");
					System.out.println("OOOOOOOOOOO" + result);
					list = new ArrayList<String>();

					if (product != null) {
						int len = product.length();
						for (int i = 0; i < len; i++) {
							list.add(product.get(i).toString());
						}
					}

					callback.onTaskComplete(list);
					for (String i : list) {
						System.out.println("XXXXX-----" + i);
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}
}
