package com.example.kenji.pdfreader;

import android.content.Context;
import android.content.res.AssetManager;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Kenji on 22/12/2016.
 */

public class QuestionDB {
    private static int total_q;
    private static ArrayList arr;
    private static String question_text;
    private static int random_num;
    private static Context ctx;
    private static int q_idx;
    private static int a_idx;

    public QuestionDB(Context ctx) {
        this.ctx = ctx;
        chooseQuestion(53);
        int max_q = StringUtils.countMatches(question_text,"Question:");

        arr = new ArrayList<>();
        for (int i=1; i<=max_q; i++){
            arr.add(i+300);//+600 for 601-712
        }
        total_q = arr.size();

    }
    private void chooseQuestion(int i){
        String q_path = "";
        /*if (i==47) {
            //q_path = "/Users/Kenji/Documents/1Z0-047";
            q_path = "1Z0-047";
        }else if (i==51){
            q_path = "1Z0-051";
        }else if (i==52){
            q_path = "1Z0-052";
        }*/
        if (i==53) {
            q_path = "1Z0-0" + i + "_301-400";
        }else {
            q_path = "1Z0-0" + i;
        }

        try {
            AssetManager am = ctx.getAssets();
            InputStream is = am.open(q_path);

            //BufferedReader br = new BufferedReader(new FileReader(q_path));
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            String NL = System.getProperty("line.separator");

            while (line != null) {
                sb.append(line + NL);
                line = br.readLine();
            }
            question_text = sb.toString();
            br.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getTotalQ(){
        return String.valueOf(total_q);
    }
    /*public void setTotalQ(int i) {
        total_q = i;
    }
    public void minusQ(int i) {
        total_q = total_q - i;
    }*/
    public void setRandom(){
        random_num = (int) (Math.random()*total_q+1);
        total_q = total_q -1;
    }
    public String getQuestion(){
        q_idx = question_text.indexOf("Question: " + arr.get(random_num-1));
        a_idx = question_text.indexOf("Answer:", q_idx+1);
        String question = question_text.substring(q_idx,a_idx);

        //System.out.print("Random is => "+q_num);
        //System.out.println(q_idx+"GETGETGET"+a_idx);

        arr.remove(random_num-1);
        return question;
    }
    public String getAnswer(){
        String answer = question_text.substring(a_idx,question_text.indexOf('\n',a_idx));
        return answer;
    }
}
