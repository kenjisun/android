package com.example.UIphone;


import java.util.Calendar;

import com.example.jogroup.R;
import com.example.service.ScheduleClient;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;


/**
 * This is the Main Activity of our app.
 * Here we allow the user to select a date,
 * we then set a notification for that date to appear in the status bar
 *  
 * @author paul.blundell
 */
public class NotificationFirst extends Activity {
	// This is a handle so that we can call methods on our service
    private ScheduleClient scheduleClient;
    // This is the date picker used to select the date for our notification
	private DatePicker picker;
	private TimePicker tpicker;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.activity_first_notification);
        
        // Create a new service client and bind our activity to this service
        scheduleClient = new ScheduleClient(this);
        scheduleClient.doBindService();

        // Get a reference to our date picker
        picker = (DatePicker) findViewById(R.id.scheduleTimePicker);
        tpicker=(TimePicker) findViewById(R.id.tp);
    }
	
    /**
     * This is the onClick called from the XML to set a new notification 
     */
    public void onDateSelectedButtonClick(View v){
    	// Get the date from our datepicker
    	int day = picker.getDayOfMonth();
    	int month = picker.getMonth();
    	int year = picker.getYear();
    	int	hourOfDay=tpicker.getCurrentHour();
    	int minute=tpicker.getCurrentMinute();
    	
   //Log.i("xxxxxxx",hourOfDay+"AAAA");
    	
    	// Create a new calendar set to the date chosen
    	// we set the time to midnight (i.e. the first minute of that day)
    	Calendar c = Calendar.getInstance();
    	c.set(year, month, day, hourOfDay, minute);
    	c.set(Calendar.HOUR_OF_DAY,hourOfDay);
    	c.set(Calendar.MINUTE,minute);
    	c.set(Calendar.SECOND, 0);
    	// Ask our service to set an alarm for that date, this activity talks to the client that talks to the service
    	scheduleClient.setAlarmForNotification(c);
    	// Notify the user what they just did

    	Toast.makeText(this, "已經完成設定: "+ year +"/"+ (month+1) +"/"+ day+"\t"+ hourOfDay+":"+ minute, Toast.LENGTH_SHORT).show();
    	/////////////////////////////
    	
    	
    
    		        
    	
    }
    
    @Override
    protected void onStop() {
    	// When our activity is stopped ensure we also stop the connection to the service
    	// this stops us leaking our activity into the system *bad*
    	if(scheduleClient != null)
    		scheduleClient.doUnbindService();
    	super.onStop();
    }
}