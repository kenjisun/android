package com.example.urlshortener;

import java.util.ArrayList;

import com.example.connect.DBUtil;

import android.app.Activity;
import android.app.ListActivity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.TextView;

public class MainActivity extends ListActivity {
	
	String[] names;
	String[] shortURLs;
	String[] longURLs;
	Button addBtn;
	Button shortenBtn;
	Button addBtn_main;
	EditText txtName;
	EditText txtURL;
	EditText txtInputSearch;
	TextView txtRowCount;
	ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
	public static int CURRENT_PAGE = 0;
	//0 = no URL; 1 = URL list; 2 = add URL; 3 = web view

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//load data from device's storage
		result = new DBUtil().loadURL(this);
		
		//determine which UI has to displayed
		if(result==null){
			setContentView(R.layout.no_url_layout);
			CURRENT_PAGE = 0;
			
			addBtn = (Button) findViewById(R.id.addURL_no);
			addBtn.setOnClickListener(new btnController(this));
		}else{
			initMainView(this,result);
			initSearchBar();
		}
		
	}


	//re-initial the views while rotating device
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		if(CURRENT_PAGE == 0){
			setContentView(R.layout.no_url_layout);
			addBtn = (Button) findViewById(R.id.addURL_no);
			addBtn.setOnClickListener(new btnController(this));
		}else if(CURRENT_PAGE == 1){
			result = new DBUtil().loadURL(this);
			initMainView(this,result);
			initSearchBar();
		}else if(CURRENT_PAGE == 2){
			setContentView(R.layout.activity_add_url);
			shortenBtn = (Button) findViewById(R.id.btnShorten);
			shortenBtn.setOnClickListener(new btnController(this));
		}
	}
	
	//initialize main view
	public void initMainView(Activity mother, ArrayList<ArrayList<String>> result){
		mother.setContentView(R.layout.activity_main);
		txtRowCount = (TextView) mother.findViewById(R.id.txt_row_count);
		addBtn_main = (Button) mother.findViewById(R.id.addURLbtn_main);
		addBtn_main.setOnClickListener(new btnController(mother));
		CURRENT_PAGE = 1;
		
		
		names = result.get(0).toArray(new String[result.get(0).size()]);
		shortURLs = result.get(1).toArray(new String[result.get(1).size()]);
		longURLs = result.get(2).toArray(new String[result.get(2).size()]);
		
		SpannableString content = new SpannableString(result.get(0).size()+"");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		txtRowCount.setText(content);
		((ListActivity) mother).setListAdapter(new CustomArrayAdapter(mother,names,shortURLs,longURLs));
		((ListActivity) mother).getListView().setOnItemClickListener(new btnController(mother,names,shortURLs,longURLs));
	}
	
	//initialize search bar
	public void initSearchBar(){
		txtInputSearch = (EditText) findViewById(R.id.inputSearch);
		txtInputSearch.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				((Filterable) getListAdapter()).getFilter().filter(arg0);
			}
			
		});
	}
	
}
