-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- 主機: localhost
-- 建立日期: May 28, 2013, 10:01 AM
-- 伺服器版本: 5.0.51
-- PHP 版本: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- 資料庫: `android_api`
-- 

-- --------------------------------------------------------

-- 
-- 資料表格式： `users`
-- 

CREATE TABLE `users` (
  `uid` int(11) NOT NULL auto_increment,
  `unique_id` varchar(23) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  PRIMARY KEY  (`uid`),
  UNIQUE KEY `unique_id` (`unique_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- 列出以下資料庫的數據： `users`
-- 

INSERT INTO `users` VALUES (1, '51a45dd4000017.34763226', 'iu', 'hh', 'cOAFOOSM2RLAWup1xr1RmkNYq/gwNmRkNWJlNmZi', '06dd5be6fb', '2013-05-28 15:33:40', NULL);
INSERT INTO `users` VALUES (2, '51a462ab53ec79.58905372', 'kk', 'abc@abc.com', 'iJH7SAUjbExzSdZD/KPsNUkJoIo1YmZhNGFmOTYw', '5bfa4af960', '2013-05-28 15:54:19', NULL);
