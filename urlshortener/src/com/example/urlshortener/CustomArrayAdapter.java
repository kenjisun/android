package com.example.urlshortener;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomArrayAdapter extends ArrayAdapter<String> {
	private Activity context;
	private String[] names;
	private String[] shortURLs;
	private String[] longURLs;
	ArrayList<ArrayList<String>> display = new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> original = new ArrayList<ArrayList<String>>();
	
	public CustomArrayAdapter(Activity context,String[] names,String[] shortURLs, String[] longURLs){
		super(context,R.layout.row_layout,names);
		this.context=context;
		this.names=names;
		this.shortURLs=shortURLs;
		this.longURLs=longURLs;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		
		if(rowView == null){
			Log.d("CustomArrayAdapter",String.valueOf(position));
			LayoutInflater inflater=context.getLayoutInflater();
			rowView=inflater.inflate(R.layout.row_layout, null,true);
		}else{
			
		}
		
		TextView txtName=(TextView)rowView.findViewById(R.id.rowName);
		TextView txtShortURL=(TextView)rowView.findViewById(R.id.rowShortURL);
		TextView txtLongURL=(TextView)rowView.findViewById(R.id.rowLongURL);
		
		txtName.setText(names[position]);
		txtShortURL.setText(shortURLs[position]);
		txtLongURL.setText(longURLs[position]);
		
		return rowView;
	}

	
	
}