package com.example.connect;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.urlshortener.MainActivity;
import com.example.urlshortener.R;
import com.example.urlshortener.btnController;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;




public class AddList extends AsyncTask<String, String, String> {

	Button addBtn_main;
	private ProgressDialog pDialog;
	Activity mother;
	private int successOrNot = 0;

	JSONParser jsonParser = new JSONParser();
	
	//------------------------------------------------
	//Table1
	String name,shortURL,longURL;
	
	

	// JSON Node names
	private static final String TAG_SUCCESS = "success";


	
	//--------------------------------------------------
	// url to create new product
	private static String url_create_product = "http://192.168.1.9/PHP/shorten_connect/insert_url.php";
	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	public AddList(Activity m,String name, String shortURL, String longURL){
		this.mother = m;
		this.name = name;
		this.shortURL = shortURL;
		this.longURL = longURL;
	}
	
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(mother);
		pDialog.setMessage("Insert into database..");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
		new GoogleShortener(longURL).execute();
	}

	/**
	 * Creating product
	 * */
	protected String doInBackground(String... args) {
		
		JSONObject json = null;
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("shortURL", GoogleShortener.resultURL));
		params.add(new BasicNameValuePair("longURL", longURL));
		
		json = jsonParser.makeHttpRequest(url_create_product,
				"POST", params);
		
		// check log cat for response
		Log.d("Create Response", json.toString());
		
		// check for success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// successfully created product
			//	Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
				//startActivity(i);
				Log.i("SUCCESS","============SUCCESS===========");
				successOrNot = 1;
				// closing this screen
				//finish();
			} else {
				System.out.println("failed to create data");
				// failed to create data
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog once done
		pDialog.dismiss();
		if(successOrNot == 1){
			Toast.makeText(mother, "Add success...", Toast.LENGTH_SHORT).show();
			mother.setContentView(R.layout.activity_main);
			addBtn_main = (Button) mother.findViewById(R.id.addURLbtn_main);
			addBtn_main.setOnClickListener(new btnController(mother));
//			Intent intent = new Intent(mother,MainActivity.class);
//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			mother.startActivity(intent);
			
		}else{
			Toast.makeText(mother, "invalid input!", Toast.LENGTH_SHORT).show();
		}
	}
	

}