/*新增的團-結束*/
package com.example.jogroup;

import java.util.ArrayList;
import java.util.UUID;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.phpsql.CreateNewProduct;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.PushService;

public class StepGroupFinish extends Activity {
	Button btnupdate;
	TextView textName,textType,textPlace,textDescription,
				textDate1,textDate2,textDate3,textDate4,textDate5;
	ArrayList<String> nameList=new ArrayList<String>();
	ArrayList<String> detailList=new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.new_group_finish);
		nameList = getIntent().getStringArrayListExtra("friends");
		init();
		setPreference();
	//////parse INIT
			Parse.initialize(this, "lYxVG0uNPWZCqg274HE8A2JGhjN2ufbbc539Dw0F",
					"dNICZdAoWELKA5a8XZXSqHtpjlTx2gOcK4DZDufe");
			ParseAnalytics.trackAppOpened(getIntent());
			PushService.setDefaultPushCallback(this, CustomTab.class);
			
			// PushService.subscribe(getBaseContext(), "invitationA",
			// MainActivity.class);
			
			//get self ID
			PushService.subscribe(getBaseContext(), "invitationA",
					CustomTab.class);
			ParseInstallation.getCurrentInstallation().saveInBackground();
		
		btnupdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UUID uuid = java.util.UUID.randomUUID();
				
				new CreateNewProduct(StepGroupFinish.this,uuid.toString(),nameList,detailList).execute();
				
				//send invited list
				ParsePush push = new ParsePush();
				push.setChannel("invitationA");
				push.setMessage(" 開團通知");
				push.sendInBackground();
				
//				v.setEnabled(false);
//		        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
//		                private ProgressDialog pd;
//		                @Override
//		                protected void onPreExecute() {
//		                         pd = new ProgressDialog(StepGroupFinish.this);
//		                         pd.setTitle("Processing...");
//		                         pd.setMessage("Please wait.");
//		                         pd.setCancelable(false);
//		                         pd.setIndeterminate(true);
//		                         pd.show();
//		                }
//		                @Override
//		                protected Void doInBackground(Void... arg0) {
//		                        try {
//		                                //Do something...
//		                               Thread.sleep(3000);
//		                        } catch (InterruptedException e) {
//		                               // TODO Auto-generated catch block
//		                               e.printStackTrace();
//		                        }
//		                        return null;
//		                 }
//		                 @Override
//		                 protected void onPostExecute(Void result) {
//		                         pd.dismiss();
//		                         findViewById(R.id.btnupdate).setEnabled(true);
//		                       //back to main
//		         				
//		         				Intent intent = new Intent(StepGroupFinish.this,CustomTab.class);
//		         				startActivity(intent);
//		                 }
//		        };
//		        task.execute((Void[])null);
				
				
				
			}
			
		});
		
	}
	
	private void setPreference() {
		SharedPreferences getPre = getSharedPreferences("DATA", 0);
		String name = getPre.getString("actName", "");
		String type = getPre.getString("attKind", "");
		String place = getPre.getString("attPlace", "");
		String description = getPre.getString("actDesc", "");
		String date1 = getPre.getString("attDate1", "") + " " + getPre.getString("attTime1", ""); 
		String date2 = getPre.getString("attDate2", "") + " " + getPre.getString("attTime2", ""); 
		String date3 = getPre.getString("attDate3", "") + " " + getPre.getString("attTime3", "");
		String date4 = getPre.getString("attDate4", "") + " " + getPre.getString("attTime4", "");
		String date5 = getPre.getString("attDate5", "") + " " + getPre.getString("attTime5", "");
		
		
		textName.setText(name);
		textType.setText(type);
		textPlace.setText(place);
		textDescription.setText(description);
		textDate1.setText(date1);
		textDate2.setText(date2);
		textDate3.setText(date3);
		textDate4.setText(date4);
		textDate5.setText(date5);
		detailList.add(name);
		detailList.add(description);
		detailList.add(place);
		detailList.add(date1+","+date2+","+date3+","+date4+","+date5);
		detailList.add(type);
	//	nameList.add(self);
	}

	private void init(){
		btnupdate = (Button) findViewById(R.id.btnupdate);
		textName = (TextView) findViewById(R.id.textName);
		textType = (TextView) findViewById(R.id.textType);
		textPlace = (TextView) findViewById(R.id.textPlace);
		textDescription = (TextView) findViewById(R.id.textDescription);
		textDate1 = (TextView) findViewById(R.id.textDate1);
		textDate2 = (TextView) findViewById(R.id.textDate2);
		textDate3 = (TextView) findViewById(R.id.textDate3);
		textDate4 = (TextView) findViewById(R.id.textDate4);
		textDate5 = (TextView) findViewById(R.id.textDate5);
	}
	
	
}