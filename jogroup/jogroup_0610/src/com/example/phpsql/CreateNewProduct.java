package com.example.phpsql;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.jogroup.CustomTab;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;




public class CreateNewProduct extends AsyncTask<String, String, String> {


	private ProgressDialog pDialog;
	private int successOrNot = 0;

	JSONParser jsonParser = new JSONParser();
	
	//------------------------------------------------
	//Table1
	String actID;
	ArrayList<String> attName,detailList;
	int attRole,ans,status;
	//Table2
	//String actID attName
	String actDesc,attPlace,aTime,attKind,attComment;
	int attResult;
	
	Activity mother;

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCT = "product";
	private static final String TAG_ACTID = "actid";
	private static final String TAG_ATTNAME = "attname";
	private static final String TAG_ATTROLE = "attrole";
	private static final String TAG_ANS = "ans";
	private static final String TAG_STATUS = "status";


	
	
	
	//--------------------------------------------------
	// url to create new product
	private static String url_create_product = "http://192.168.0.32/android_connect/create_product.php";
	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	public CreateNewProduct(Activity m,String actID,ArrayList<String> attName,ArrayList<String> detailList){
		this.mother = m;
		this.actID = actID;
		this.attName = attName;
		this.detailList = detailList;
	}
	
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(mother);
		pDialog.setMessage("Creating Product..");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}

	/**
	 * Creating product
	 * */
	protected String doInBackground(String... args) {
		
		
//		String id = actID;
//		String att_name = attName;
//		String role=String.valueOf(attRole);
//		String feedback=String.valueOf(ans);
//		String att_status=String.valueOf(status);
		JSONObject json = null;
		JSONObject json2 = null;

		for(int i=0;i<attName.size();i++){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("actid", actID));
		params.add(new BasicNameValuePair("attname", attName.get(i)));
			if(i==0){
				params.add(new BasicNameValuePair("attrole", "1"));
			}else{
				params.add(new BasicNameValuePair("attrole", "0"));
			}
		params.add(new BasicNameValuePair("ans", "0"));
		params.add(new BasicNameValuePair("status", "0"));
		// getting JSON Object
		// Note that create product url accepts POST method
		json = jsonParser.makeHttpRequest(url_create_product,
				"POST", params);
		
		// check log cat for response
		Log.d("Create Response", json.toString());
		}
	
		List<NameValuePair> params2 = new ArrayList<NameValuePair>();
		params2.add(new BasicNameValuePair("actid", actID));
		params2.add(new BasicNameValuePair("actname", detailList.get(0)));//list[0]
		params2.add(new BasicNameValuePair("actdesc", detailList.get(1)));
		params2.add(new BasicNameValuePair("attplace", detailList.get(2)));
		params2.add(new BasicNameValuePair("atime", detailList.get(3)));
		params2.add(new BasicNameValuePair("attkind", detailList.get(4)));
		params2.add(new BasicNameValuePair("attcomment", "0"));
		params2.add(new BasicNameValuePair("attresult", "0"));
		json2 = jsonParser.makeHttpRequest(url_create_product,
				"POST", params2);
		
		// check for success tag
		try {
			int success = json.getInt(TAG_SUCCESS);
			int success2 = json2.getInt(TAG_SUCCESS);

			if (success == 1 && success2 == 1) {
				// successfully created product
			//	Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
				//startActivity(i);
				Log.i("SUCCESS","============SUCCESS===========");
				successOrNot = 1;
				// closing this screen
				//finish();
			} else {
				System.out.println("failed to create xxxxxx");
				// failed to create product
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog once done
		pDialog.dismiss();
		if(successOrNot == 1){
			Intent intent = new Intent(mother,CustomTab.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			mother.startActivity(intent);
		}
	}

}