package com.example;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import sun.misc.IOUtils;

public class PDFsubstr {
    public static void main(String [] args) {
        PDFParser parser = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        PDFTextStripper pdfStripper;

        String parsedText;
        String fileName = "/Users/Kenji/1Z0-053_after100_copy";
        File file = new File(fileName);

        try {
            /*
            parser = new PDFParser(new RandomAccessFile(file,"r"));
            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = neaw PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            parsedText = pdfStripper.getText(pdDoc);
            */
            //System.out.println(parsedText.replaceAll("!"," "));

            /* erase all comment between Question: and Answer:*/
            //String testString = "Question: 1.ARRRR  Answer: DFE  \n This is comm1 Question: 2.BRRRR Answer: ABC \n This is comm2  Question: 3.CRRRR Answer: E \n This is comm3";
            parsedText = readTextFromFile("/Users/Kenji/1Z0-053_601-712raw");
            int q_idx = 0;
            int a_idx = 0;
            int q_occ = 1;
            int a_occ = 0;
            while (q_idx>=0 && a_idx<=q_idx) {
                q_occ = q_occ+1;
                a_occ = a_occ+1;
                q_idx = ordinalIndexOf(parsedText,"Question: ",q_occ);
                a_idx = ordinalIndexOf(parsedText,"Answer:",a_occ) + parsedText.substring(ordinalIndexOf(parsedText,"Answer:",a_occ)).indexOf('\n')+1;
                //System.out.println(q_idx);
                //System.out.println(a_idx);
                if (q_idx < 0 || a_idx < 0){
                    break;
                }
                parsedText = parsedText.substring(0,a_idx)+parsedText.substring(q_idx);
                //System.out.println(testString);
            }

            System.out.println(parsedText);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (cosDoc != null)
                    cosDoc.close();
                if (pdDoc != null)
                    pdDoc.close();
            } catch (Exception e1) {
                e.printStackTrace();
            }

        }
    }

    public static int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }

    private static String readTextFromFile(String path) {
        BufferedReader br = null;
        String everything = "";

        try {
            br = new BufferedReader(new FileReader(path));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            everything = sb.toString();
            br.close();
        } catch (Exception e){
            try {
                br.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return everything;
    }
}
