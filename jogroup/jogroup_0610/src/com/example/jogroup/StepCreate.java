﻿/*Tab三畫面之一-我開的團1*/
package com.example.jogroup;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.phpsql.CustomHttpClient;
import com.example.phpsql.GetProductDetails;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class StepCreate extends Activity {
	TextView ans;
	ListView MyGroupList;
	String[] finaldata;

	// /////////////////////===== SharedPreferences
	private SharedPreferences settings;
	private static final String data = "DATA";
	private static final String nameField = "actName";
	private static final String descField = "actDesc";
	private static final String placeField = "attPlace";
	private static final String kindField = "attKind";

	// //////////////////////=====交換資料用
	//static StepCreate instance;
	static ArrayList<JSONObject> json_data_List = new ArrayList<JSONObject>();

	// //////////////////////=====listview
	ArrayList<HashMap<String, Object>> goods = new ArrayList<HashMap<String, Object>>();
	String[] attkind, actname, attplace;
	String actid = "bb";// user's actid
	String[] ans_count = { "0" };

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//instance = this;
		
		super.onCreate(savedInstanceState);
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork() // StrictMode
																		// is
																		// most
																		// commonly
																		// used
																		// to
																		// catch
																		// accidental
																		// disk
																		// or
																		// network
																		// access
																		// on
																		// the
																		// application's
																		// main
																		// thread
				.penaltyLog().build());
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_create);
		init();
		MyGroupList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long i) {
				Intent intent = new Intent(StepCreate.this, DirectorTool.class);
				startActivity(intent);

			}
		});

	}

	public void init() {
		MyGroupList = (ListView) findViewById(R.id.MyGroupList);
		ans = (TextView) findViewById(R.id.txt_ans);
		MyGroupList.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				read_HttpGet();
				read_HttpPost();
			}
		});

		// read_HttpGet();

	}

	// public void read(){

	// String my_name_data;
	// settings = getSharedPreferences(data,0);
	// my_name_data=settings.getString(nameField, "");
	// finaldata = my_name_data.split(",");
	// Log.e("mydata",finaldata+"");
	// ArrayAdapter<String> adapter=new
	// ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,finaldata);
	// MyGroupList.setAdapter(adapter);

	// }

	private void read_HttpPost() {

		String response = null;

		// call executeHttpPost method passing necessary parameters
		try {
			response = CustomHttpClient
					.executeHttpPost("http://192.168.0.32/android_connect/android_connect_db.php");

			// store the result returned by PHP script that runs MySQL query
			String result = response.toString();

			Log.i("sam2", result + "");

			// parse json data
			try {
				
				JSONArray jArray = new JSONArray(result);

				Log.i("sam3", "this is a " + jArray);

				for (int i = 0; i < jArray.length(); i++) {

					JSONObject json_data = jArray.getJSONObject(i);
					Log.i("sam4", "loop : " + i);

					// // /////////////////////============

					HashMap<String, Object> item = new HashMap<String, Object>();
					item.put("attkind", json_data.getString("attkind"));
					item.put("actname", json_data.getString("actname"));
					item.put("attplace", json_data.getString("attplace"));
					item.put("ans_count", ans_count[0]);
					json_data.put("ans_count", ans_count[0]);
					Log.i("sam5", item + "");

					goods.add(item);
					json_data_List.add(json_data);
				}
				Log.i("sam6", goods + "");

				SimpleAdapter myAdapter2 = new SimpleAdapter(getBaseContext(),
						goods, R.layout.row, new String[] { "attkind",
								"actname", "attplace", "ans_count" },
						new int[] { R.id.text_attkind, R.id.text_actname,
								R.id.text_attplece, R.id.txt_ans });

				// /////////////=============

				MyGroupList.setAdapter(myAdapter2);

			}

			catch (JSONException e) {
				Log.e("log_tag", "Error parsing data " + e.toString());
			}

		}

		catch (Exception e) {
			Log.e("log_tag", "Error in http connection!!" + e.toString());
		}

	}

	// //////////////get///////////////

	private void read_HttpGet() {

		String get_response = null;

		// call executeHttpPost method passing necessary parameters
		try {
			get_response = CustomHttpClient
					.executeHttpGet("http://192.168.0.32/android_connect/get_result.php?actid="
							+ actid);

			// store the result returned by PHP script that runs MySQL query
			String get_result = get_response;

			Log.i("get_sam===========", get_result + "");
			try {
				Log.i("get_in", " in try");
				JSONObject jobject = new JSONObject(get_result);
				Log.i("get_sam2", get_result + "");
				JSONArray jArray = (JSONArray) jobject.getJSONArray("product").get(0);
				Log.d("fy",jArray.toString());
				Log.d("fy",jArray.get(0).toString());
				Log.d("fy",jArray.get(1).toString());
				Log.d("fy",jArray.get(2).toString());
				Log.d("fy",jArray.get(3).toString());
				Log.d("fy",jArray.get(4).toString());

				ans_count[0] = String.valueOf(jobject.getInt("count"));
				Log.i("get_sam3", jobject.getInt("count") + "");

			} catch (JSONException e) {
				Log.e("log_tag", "get_Error parsing data " + e.toString());
			}

		}

		catch (Exception e) {
			Log.e("log_tag", "get_Error in http connection!!" + e.toString());
		}

	}

	@Override
	protected void onDestroy() {
		json_data_List.clear();
		super.onDestroy();
	}

}
