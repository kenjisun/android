package com.example.jogroup;
//匯入DB_Constant
import static android.provider.BaseColumns._ID;
import static com.example.jogroup.DB_Constant.DB_TABLE_NAME;
import static com.example.jogroup.DB_Constant.DB_NAME;
import static com.example.jogroup.DB_Constant.DB_OTHER;

import static com.example.jogroup.DB_Constant.DB_TABLE_NAME2;
import static com.example.jogroup.DB_Constant.DB_TIME;


import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class SQLiteActivity extends Activity {
	private MySQLite mysqlite=null;
	EditText editid,editname,editother;
	ListView show;
	Button add,update,delete;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sqlite);
		mysqlite = new MySQLite(this);
		editid = (EditText)findViewById(R.id.id);
		editname = (EditText)findViewById(R.id.name);
		editother = (EditText)findViewById(R.id.other);
//		show = (ListView)findViewById(R.id.show);
		add = (Button)findViewById(R.id.add);
		update = (Button)findViewById(R.id.update);
		delete = (Button)findViewById(R.id.delete);
		
//		showlist();
		
		add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SQLiteDatabase db = mysqlite.getWritableDatabase(); //開啟資料庫並打開寫入權限
				//將資料放入content中
				ContentValues content=new ContentValues();
				content.put(DB_NAME, editname.getText().toString());
				content.put(DB_OTHER, editother.getText().toString());
				
				db.insert(DB_TABLE_NAME, null, content);
				cleanEditText();
		//		showlist();
			}
		});
		
		delete.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				String id = editid.getText().toString();
//		    	String name = editname.getText().toString();
//		    	String other = editother.getText().toString();
				
//		    	SQLiteDatabase db = mysqlite.getWritableDatabase();
//		    	db.delete(DB_TABLE_NAME, _ID + "=" + id, null);
//		    	db.delete(DB_TABLE_NAME, DB_NAME + "=" + name, null);
//		    	db.delete(DB_TABLE_NAME, "DB_OTHER=" + other, null);
		    	
//		    	cleanEditText();
			}
		});
	}
	
//	private void openDatabase(){
//		mysqlite = new MySQLite(this);
//	}
	
	private Cursor getCursor(){	
		SQLiteDatabase db = mysqlite.getReadableDatabase();
		String[] columns ={_ID,DB_NAME,DB_OTHER};
		Cursor cursor=db.query(DB_TABLE_NAME,columns,null,null,null,null,null);
		startManagingCursor(cursor);	
		return cursor;	
	}
	Cursor cursor;
/*	private void showlist(){
		
		cursor=getCursor();
		String[] from={DB_NAME,DB_OTHER};
		int[] to={R.id.textname,R.id.textother};
	
		@SuppressWarnings("deprecation")
		SimpleCursorAdapter adapter=new SimpleCursorAdapter(this,R.layout.row,cursor,from,to);
		show.setAdapter(adapter);
			
	}*/
	
	private void cleanEditText(){
	//	id.setText("");
		editname.setText("");
    	editother.setText("");
    }

}
