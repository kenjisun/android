/*新增的團-設定時間*/
package com.example.jogroup;

import java.util.StringTokenizer;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class StepNewOneTime extends Activity implements OnClickListener{
	Button btnnexttime;
	ImageButton settime1;
	TextView time1,time2,time3,time4,time5;
	
	private ImageButton btn_date1,btn_date2,btn_date3,btn_date4,btn_date5;
	private TextView tv_date1,tv_date2,tv_date3,tv_date4,tv_date5;
	public String Date1,Date2,Date3,Date4,Date5;
	
	private SharedPreferences settings;
    private static final String data = "DATA";
    private static final String timeField1 = "attTime1";
    private static final String timeField2 = "attTime2";
    private static final String timeField3 = "attTime3";
    private static final String timeField4 = "attTime4";
    private static final String timeField5 = "attTime5";
    
    static final String[] TimeField = {
    		"attTime1","attTime2","attTime3","attTime4","attTime5"
    };
        
    public TimePickerDialog timePickDialog = null;
        
    View layout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView( layout = getLayoutInflater().inflate(R.layout.new_group_time, null) );
		init();		
	}
	
	public void init()
	{
		btnnexttime = (Button) findViewById(R.id.btnNextTime);
		
		time1 =(TextView) findViewById(R.id.time1);
		time2 =(TextView) findViewById(R.id.time2);
		time3 =(TextView) findViewById(R.id.time3);
		time4 =(TextView) findViewById(R.id.time4);
		time5 =(TextView) findViewById(R.id.time5);
	    settime1 =(ImageButton)findViewById(R.id.settime1);
	    
	    btn_date1 = (ImageButton) findViewById(R.id.btn_date1);
		btn_date2 = (ImageButton) findViewById(R.id.btn_date2);
		btn_date3 = (ImageButton) findViewById(R.id.btn_date3);
		btn_date4 = (ImageButton) findViewById(R.id.btn_date4);
		btn_date5 = (ImageButton) findViewById(R.id.btn_date5);
		
		tv_date1 = (TextView) findViewById(R.id.tv_date1);
		tv_date2 = (TextView) findViewById(R.id.tv_date2);
		tv_date3 = (TextView) findViewById(R.id.tv_date3);
		tv_date4 = (TextView) findViewById(R.id.tv_date4);
		tv_date5 = (TextView) findViewById(R.id.tv_date5);
	
		btn_date1.setOnClickListener(this);
		btn_date2.setOnClickListener(this);
		btn_date3.setOnClickListener(this);
		btn_date4.setOnClickListener(this);
		btn_date5.setOnClickListener(this);
			
	    settings = getSharedPreferences(data,0);
	}
	
	public void next(View v){
		save();
		Intent intent = new Intent(StepNewOneTime.this,StepNewOneNameList.class);
		startActivity(intent);
	}
	
	public void clean(View v){
		time1.setText("");
		time2.setText("");
		time3.setText("");
		time4.setText("");
		time5.setText("");
	}
	
	public void onClick(View v){
		settings = getSharedPreferences(data, 0);
		switch(v.getId()) {
	    case R.id.btn_date1:
	    	toCalendar();
	    	settings.edit()/*.putInt("textViewID", tv1.getId());/*/.putString("datebtn","btn_date1").commit();
	        break;
	    case R.id.btn_date2:
	    	toCalendar();
	    	settings.edit()/*.putInt("textViewID", tv2.getId());/*/.putString("datebtn","btn_date2").commit();
	        break;
	    case R.id.btn_date3:
	    	toCalendar();
	    	settings.edit()/*.putInt("textViewID", tv3.getId());/*/.putString("datebtn","btn_date3").commit();
	        break;
	    case R.id.btn_date4:
	    	toCalendar();
	    	settings.edit()/*.putInt("textViewID", tv4.getId());/*/.putString("datebtn","btn_date4").commit();
	        break;
	    case R.id.btn_date5:
	    	toCalendar();
	    	settings.edit()/*.putInt("textViewID", tv5.getId());/*/.putString("datebtn","btn_date5").commit();
	        break;
	    default :
			int timeFieldIndex = Integer.valueOf(v.getTag().toString());
			TextView tv = (TextView) layout.findViewWithTag("time"+timeFieldIndex);
			String time = tv.getText().toString();
	 		
	 		if (time != null && !time.equals("")) {
	 			StringTokenizer st = new StringTokenizer(time, ":");
	            String timeHour = st.nextToken();
	            String timeMinute = st.nextToken();
	
	             timePickDialog = new TimePickerDialog(v.getContext(),
	                     new TimePickHandler(tv,timeFieldIndex), Integer.parseInt(timeHour),
	                     Integer.parseInt(timeMinute), true);
	 		}
	 		else{
	 			timePickDialog = new TimePickerDialog(v.getContext(),
	                     new TimePickHandler(tv,timeFieldIndex), 10, 45, true);
	 		}
	 		timePickDialog.show();
	    }
	 }
	
	 public class TimePickHandler implements OnTimeSetListener {
		 final TextView tv;
		 final String timeField;
		 TimePickHandler(TextView tv,int timeFieldIndex){
			 this.tv = tv;
			 this.timeField = TimeField[timeFieldIndex-1];
		 }

	     @Override
	     public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	     tv.setText(hourOfDay + ":" + minute);
//	            settings
//	            .edit()
//	            .putString(timeField, tv.getText().toString())
//	            .commit();
	     timePickDialog.hide();
	     }
	 }
	 
	 public void save(){
		 settings
        .edit()
		.putString(timeField1,time1.getText().toString())
		.putString(timeField2,time2.getText().toString())
		.putString(timeField3,time3.getText().toString())
		.putString(timeField4,time4.getText().toString())
		.putString(timeField5,time5.getText().toString())
		.commit();
	 }
	 
	 public void readPre(){
			//***讀取preference***//
		 	settings = getSharedPreferences(data,0);
			String datebtn = settings.getString("datebtn","");
			if(datebtn=="btn_date1"){
				String date1 = settings.getString("attDate1","");
				tv_date1.setText(date1);
			}
			else if(datebtn=="btn_date2"){
				String date2 = settings.getString("attDate2","");
				tv_date2.setText(date2);
			}
			else if(datebtn=="btn_date3"){
				String date3 = settings.getString("attDate3","");
				tv_date3.setText(date3);
			}
			else if(datebtn=="btn_date4"){
				String date4 = settings.getString("attDate4","");
				tv_date4.setText(date4);
			}
			else if(datebtn=="btn_date5"){
				String date5 = settings.getString("attDate5","");
				tv_date5.setText(date5);
			}
			//findViewById(preferences.getInt("textViewID", tv1.getId())
		}
		
		public void toCalendar(){
			Intent intent = new Intent();
			intent.setClass(StepNewOneTime.this,StepCalendar.class);
			startActivity(intent);
		}
		
		@Override
		protected void onResume() {
			super.onResume();
			Log.e("onResume:","==mainOnResume==");
			readPre();
		}
}