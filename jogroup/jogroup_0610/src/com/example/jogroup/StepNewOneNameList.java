/*新增一團-邀請人員清單*/
package com.example.jogroup;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.phpsql.GetProductDetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class StepNewOneNameList extends Activity {
	Button BtnNextName;
	ListView lv;
	ArrayList<String> friendsList = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.new_group_name_list);
		init();
		
		
		
		
		BtnNextName.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String item;
				int len = lv.getCount();
				Log.i("kkkkkkkkkkLIST","length"+len);
				SparseBooleanArray checked = lv.getCheckedItemPositions();
				for (int i = 0; i < len; i++){
					boolean tof = checked.get(i);
					if(tof){
						item = GetProductDetails.Name[i];
						friendsList.add(item);
						Log.i("kkkkkkkkkkLIST","keyAt="+checked.keyAt(i)+"KKK"+checked.get(i)+"CCC"+checked.valueAt(i));
						/* do whatever you want with the checked item */
					}
				}
				Intent intent = new Intent(StepNewOneNameList.this,StepGroupFinish.class);
				intent.putStringArrayListExtra("friends", friendsList);
				startActivity(intent);
			}
		});
		
	}


	private void init(){
		BtnNextName = (Button) findViewById(R.id.BtnNextName);
		lv = (ListView) findViewById(R.id.MyInvitationList);
		lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		lv.post(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new GetProductDetails(StepNewOneNameList.this,lv).execute();
				
			}});
	}
	
	
}