package com.example.androidhive;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

public class UpdateDB  {
	String attname,actid;
    int ans;
    //String type;
    //String update_content;
	
	

	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	

	// url to update product
	private static final String url_update_product = "http://10.0.2.2/android_connect/update_product.php";
	
	// url to delete product
	private static final String url_delete_product = "http://10.0.2.2/android_connect/delete_product.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCT = "product";
	private static final String TAG_ACTID = "actid";
	private static final String TAG_ATTNAME = "attname";
	private static final String TAG_ATTROLE = "attrole";
	private static final String TAG_ANS = "ans";
	private static final String TAG_STATUS = "status";
	
	
	public UpdateDB(String id, String name, int answer){
		actid=id;
		attname=name;
		ans=answer;
		
	}
	

	

		public void updateDB(){
				// starting background task to update product
				new SaveProductDetails().execute();
				//int result;
				// Handler handler=new Handler();
			//	handler.handleMessage(msg)
			
		}


	/**
	 * Background Async Task to  Save product Details
	 * */
	class SaveProductDetails extends AsyncTask<String, Void, Void> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		
		/**
		 * Saving product
		 * @return 
		 * */
		protected Void doInBackground(String... args) {

			// getting updated data from EditTexts
			
			Message message=new Message();
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_ACTID, actid));
			params.add(new BasicNameValuePair(TAG_ATTNAME, attname));
			//params.add(new BasicNameValuePair(TAG_ATTROLE, role));
			params.add(new BasicNameValuePair(TAG_ANS, String.valueOf(ans)));

			// sending modified data through http request
			// Notice that update product url accepts POST method
			JSONObject json = jsonParser.makeHttpRequest(url_update_product,
					"GET", params);
			int success=3;
			// check json success tag
			try {
				int result = json.getInt(TAG_SUCCESS);
				
				if (result == 1) {
					// successfully updated
					success=result;
				} else {
					success=0;// failed to update product
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			//return success;
			message.what=success;
		//	handler.sendMessage(message);
			return null;
		}
		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(int result) {
			// dismiss the dialog once product uupdated
			
		}
	}
}
		
	

