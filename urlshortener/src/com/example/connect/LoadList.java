package com.example.connect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;


public class LoadList extends AsyncTask<String, String, JSONArray> {

	public ProgressDialog pDialog;

	JSONParser jsonParser = new JSONParser();
	String actID="bb",attName="dd";
	int attRole=0,ans=0,status=0;
	String query_actID,query_attName;
	int query_attRole,query_ans,query_status;
	
	
	// JSON Node names
		public static final String TAG_SUCCESS = "success";
		public static final String TAG_PRODUCT = "product";
		public static final String TAG_ACTID = "actid";
		public static final String TAG_ATTNAME = "attname";
		public static final String TAG_ATTROLE = "attrole";
		public static final String TAG_ANS = "ans";
		public static final String TAG_STATUS = "status";
	
	
	private static final String url_product_detials = "http://192.168.1.9/PHP/android_connect/get_name_acc_list.php";
	
		Activity mother;
		ListView lv;
	
	public LoadList(Activity m,ListView lv){
		this.mother = m;
		this.lv = lv;
	}
	
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(mother);
		pDialog.setMessage("Loading friends list. Please wait...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}

	protected JSONArray doInBackground(String... arg) {

		// updating UI from Background Thread
		//runOnUiThread(new Runnable() {
			//public void run() {
				// Check for success tag
				int success;
				JSONArray productObj;
				JSONObject json;
				try {
					// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
					//System.out.println("arg[0]====="+arg[0]);
				params.add(new BasicNameValuePair("getList", "iNeedList"));
					//System.out.println("params========="+params);
					// getting product details by making HTTP request
					// Note that product details url will use GET request
					json = jsonParser.makeHttpRequest(
							url_product_detials, "POST", params);
					System.out.println("JSON========="+json);

					// check your log for json response
	
					
					// json success tag
					success = json.getInt(TAG_SUCCESS);
					//System.out.println(success);
					if (json != null) {
						// successfully received product details
						productObj = json
								.getJSONArray("result"); // JSON Array
						
						// get first product object from JSON Array
						//JSONObject product = productObj.getJSONObject(0);

						// product with this pid found
						// Edit Text
					//	txtName = (EditText) findViewById(R.id.inputName);
					//	txtPrice = (EditText) findViewById(R.id.inputPrice);
					//	txtDesc = (EditText) findViewById(R.id.inputDesc);

						// display product data in EditText
						//txtDesc.setText(product.getString(TAG_ACTID));
						//txtName.setText(product.getString(TAG_ATTNAME));
						//txtPrice.setText(product.getString(TAG_ATTROLE));
						//txtDesc.setText(product.getString(TAG_ANS));
						//txtDesc.setText(product.getString(TAG_STATUS));
						return productObj;

					}//else{
						// product with pid not found
					//}
				} catch (JSONException e) {
					e.printStackTrace();
				}
		//	}
//		});

		return null;
	}


	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(JSONArray result) {
		// dismiss the dialog once got all details
		try{
			if(result!=null){
				JSONObject product;
			//JSONObject product = result.getJSONObject(0);
			//System.out.println("=.=..=.=.=.=.=.=.=.=.="+product.toString());
			//System.out.println("+_+__+_+_+_+_++__+_+_+++_+_"+result);
			String[] name = new String[result.length()]; 
			String[] account = new String[result.length()];  
			for(int i =0;i<result.length();i++){
				product = result.getJSONObject(i);
				name[i]= product.getString("name");
				account[i]= product.getString("account");
			
			}
			
			
			Name = name;
			PhoneNumber = account;
			showlist();
//			query_attRole=Integer.parseInt(product.getString(TAG_ATTROLE));
//			query_ans=Integer.parseInt(product.getString(TAG_ANS));
//			query_status=Integer.parseInt(product.getString(TAG_STATUS));
//			System.out.println("actid: "+query_actID);
//			System.out.println("attname: "+query_attName);
//			System.out.println("attrole: "+query_attRole);
//			System.out.println("Answer: "+query_ans);
//			System.out.println("status: "+query_status);
			}
			
		}catch(JSONException e) {
					e.printStackTrace();
		}
		pDialog.dismiss();
	}
	
	
	public void showlist(){
		ArrayList<HashMap<String, Object>> outputArray = new ArrayList<HashMap<String, Object>>();

//		for (int i = 0; i < Name.length; i++) {
//			HashMap<String, Object> item = new HashMap<String, Object>();
//			item.put("name", Name[i]);
//			item.put("phonenumber", PhoneNumber[i]);
//			outputArray.add(item);
//		}
		
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(mother, android.R.layout.simple_list_item_multiple_choice,Name);
		
//		SimpleAdapter myAdapter2 = new SimpleAdapter(mother, outputArray,
//				R.layout.row_namelist, new String[] { "name", "phonenumber" },
//				new int[] { R.id.namelist, R.id.phonelist });

		this.lv.setAdapter(adapter);
		
		
	}
	
	public static String[] Name;
	public static String[] PhoneNumber;
	
}