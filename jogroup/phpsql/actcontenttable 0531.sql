-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生日期: 2013 年 05 月 31 日 06:19
-- 伺服器版本: 5.5.24-log
-- PHP 版本: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `activitydb`
--

-- --------------------------------------------------------

--
-- 表的結構 `actcontenttable`
--

CREATE TABLE IF NOT EXISTS `actcontenttable` (
  `actid` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `actname` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `actdesc` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attplace` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `atime` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attkind` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attcomment` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `attresult` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 轉存資料表中的資料 `actcontenttable`
--

INSERT INTO `actcontenttable` (`actid`, `actname`, `actdesc`, `attplace`, `atime`, `attkind`, `attcomment`, `attresult`) VALUES
('a1', 'bb', '中文', 'ddd', 'ffff', 'ggg', 'ww', 1),
('bb', 'xxx----', 'ä¸­ ', 'taipei ', '2010 ', '111', 'yyyy ', 2),
('3e657e3d-1d47-4c0c-b', '????', 'this is a description', 'Taipei', '20131001-10:00,20131002-18:00', '??', 'have fun together', 0),
('3f9e6e81-f621-43ab-8', 'eventTitle', 'this is a description', 'Taipei', '20131001-10:00,20131002-18:00', 'Dinning', 'have fun together', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
