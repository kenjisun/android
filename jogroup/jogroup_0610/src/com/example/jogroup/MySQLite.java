package com.example.jogroup;

import android.content.Context;
//匯入DB_Constant
import static android.provider.BaseColumns._ID;
import static com.example.jogroup.DB_Constant.DB_TABLE_NAME;
import static com.example.jogroup.DB_Constant.DB_NAME;
import static com.example.jogroup.DB_Constant.DB_OTHER;

import static com.example.jogroup.DB_Constant.DB_TABLE_NAME2;
import static com.example.jogroup.DB_Constant.DB_TIME;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public  class MySQLite extends SQLiteOpenHelper
{
	//定義資料庫名稱
	private final static String DATABASE_NAME = "act.db";
	//資料庫版本
	private static final int VERSION = 1;
	
	public MySQLite(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		//創建table與資料表
		final String CREATE_TABLE = "CREATE TABLE " + DB_TABLE_NAME + " ("			//建立table
									+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 	//設定id為primary key且自動遞增
									+DB_NAME + " VARCHAR(20), " 					//新增欄位
									+DB_OTHER + " VARCHAR(100));" ;					//新增欄位
		
		//執行sql指令
		db.execSQL(CREATE_TABLE);
		

	}
	
	//更新SQLite
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
}