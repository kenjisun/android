package com.example.kenji.pdfreader;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final QuestionDB QDB = new QuestionDB(this.getApplicationContext());

        final TextView txt_q = (TextView) findViewById(R.id.tv1);
        final TextView txt_a = (TextView) findViewById(R.id.tv2);
        final TextView txt_count = (TextView) findViewById(R.id.tv_count);
        final Button btn_next = (Button) findViewById(R.id.button1);
        final Button btn_answer = (Button) findViewById(R.id.button2);
        btn_answer.setEnabled(false);

        btn_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txt_a.setVisibility(View.INVISIBLE);
                btn_answer.setEnabled(true);
                QDB.setRandom();
                txt_q.setText(QDB.getQuestion());
                txt_count.setText(QDB.getTotalQ());
                if (QDB.getTotalQ().equals("0")){
                    btn_next.setEnabled(false);
                }
            }
        });

        btn_answer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txt_a.setVisibility(View.VISIBLE);
                txt_a.setText(QDB.getAnswer());
            }
        });
    }

}
