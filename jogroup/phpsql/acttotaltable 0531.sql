-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生日期: 2013 年 05 月 31 日 06:19
-- 伺服器版本: 5.5.24-log
-- PHP 版本: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `activitydb`
--

-- --------------------------------------------------------

--
-- 表的結構 `acttotaltable`
--

CREATE TABLE IF NOT EXISTS `acttotaltable` (
  `actid` varchar(20) CHARACTER SET latin1 NOT NULL,
  `attname` varchar(15) CHARACTER SET latin1 NOT NULL,
  `attrole` int(11) NOT NULL,
  `ans` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 轉存資料表中的資料 `acttotaltable`
--

INSERT INTO `acttotaltable` (`actid`, `attname`, `attrole`, `ans`, `status`) VALUES
('3f9e6e81-f621-43ab-8', 'john', 1, 66, 0),
('3f9e6e81-f621-43ab-8', 'mary', 0, 0, 0),
('3f9e6e81-f621-43ab-8', 'lauliang', 0, 0, 0),
('bb', 'xxx----', 1, 0, 1),
('bb', 'xxx----', 1, 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
