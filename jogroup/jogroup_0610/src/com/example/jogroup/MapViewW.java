package com.example.jogroup;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapViewW extends FragmentActivity implements OnClickListener,
		LocationListener {

	Geocoder geocoder = null;
	private GoogleMap map;
	double lat, lng;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		findViews();

//		mapbutton.setOnClickListener(new Button.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				MapViewW.this.finish();
//
//			}
//		});

		Bundle bundle = getIntent().getExtras();
		KEY_A = bundle.getString("Key_Address");

		List<Address> addresses = null;
		Address address = null;

		geocoder = new Geocoder(this, Locale.getDefault());
		try {

			addresses = geocoder.getFromLocationName(KEY_A, 1);
		//	Toast.makeText(this, "address:" + addresses, Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (addresses == null || addresses.isEmpty()) {

		} else
			address = addresses.get(0);
		double lat = address.getLatitude();
		double lng = address.getLongitude();

		mapview.setText(KEY_A);

		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();

		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng),
				15));

//		Toast.makeText(this, "緯度:" + lat, Toast.LENGTH_SHORT).show();
//		Toast.makeText(this, "經度:" + lng, Toast.LENGTH_LONG).show();

		map.addMarker(new MarkerOptions().position(new LatLng(lat, lng))
				.title(KEY_A)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.park)));

	}

	// setListensers();

//	private Button mapbutton;
	private TextView mapview;
	private String KEY_A;

	private void findViews() {

//		mapbutton = (Button) findViewById(R.id.MapButton);
		mapview = (TextView) findViewById(R.id.MapText);

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

}
