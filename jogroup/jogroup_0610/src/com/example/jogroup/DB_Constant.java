package com.example.jogroup;

import android.provider.BaseColumns;

public interface DB_Constant extends BaseColumns {

	public static final String DB_TABLE_NAME = "family";	
	public static final String DB_NAME = "name";
	public static final String DB_OTHER = "other";
	
	public static final String DB_TABLE_NAME2 = "group";
	public static final String DB_TIME ="time";

}