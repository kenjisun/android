package com.example.connect;

import java.util.ArrayList;

import com.example.urlshortener.MainActivity;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

public class DBUtil {
	
	private static SQLiteDatabase db;
	private static final String PATH = "/urlDB";
	private static Cursor cursor;
	
	public ArrayList<ArrayList<String>> loadURL(Activity mother) {
		
		
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> shortURLs = new ArrayList<String>();
		ArrayList<String> longURLs = new ArrayList<String>();
		
		try {
			db=SQLiteDatabase.openDatabase
				(
					mother.getFilesDir().getPath()+PATH, 
					null, 
					SQLiteDatabase.OPEN_READWRITE|SQLiteDatabase.CREATE_IF_NECESSARY
				);
			String sql="create table if not exists url_list(id integer primary key,name varchar(20),shortURL text, longURL text);";
			db.execSQL(sql);
			cursor=db.query("url_list", null, null, null, null, null, "id"+" DESC");
			int iRow=cursor.getCount();
			//int iColumn=cursor.getColumnCount();
			//cursor.moveToFirst();
				if(iRow>0){
					while(cursor.moveToNext()){
						
							names.add(cursor.getString(1));
							shortURLs.add(cursor.getString(2));
							longURLs.add(cursor.getString(3));
						
					}
					
					result.add(names);
					result.add(shortURLs);
					result.add(longURLs);
					db.close();
					cursor.close();
					return result;
				}
			
			db.close();
			cursor.close();
		}catch(Exception e) {
			Toast.makeText(mother, "Load table error�G"+e.toString(), Toast.LENGTH_LONG).show();
		}
		Toast.makeText(mother, "Database is empty", Toast.LENGTH_LONG).show();
		return null;
	}
	
	
	public boolean insertURL(Activity mother,String inputName, String inputURL){
		
		try {
			db=SQLiteDatabase.openDatabase
				(
					mother.getFilesDir().getPath()+PATH, 
					null, 
					SQLiteDatabase.OPEN_READWRITE				
				);
			cursor=db.query("url_list", null, null, null, null, null, null);
			ContentValues cv = new ContentValues();
			cv.put("name", inputName);
			cv.put("shortURL", GoogleShortener.resultURL);
			cv.put("longURL", inputURL);
			
			long result = db.insert("url_list", null, cv);
			if(result != -1L){
				return true;
			}
			
		}catch(Exception e) {
			
		}finally {
			cursor.close();
			db.close();
		}
		return false;
	}
	
}