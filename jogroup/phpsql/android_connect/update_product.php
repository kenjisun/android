<?php

/*
 * Following code will update a product information
 * A product is identified by product id (pid)
 */

// array for JSON response
$response = array();

// check for required fields
if (isset($_GET['actid']) && isset($_GET['attname']) && isset($_GET['ans'])) {
    
    $actid = $_GET['actid'];
    $attname = $_GET['attname'];
    $ans = $_GET['ans'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';
	//echo $ans;
	//echo $attname;

    // connecting to db
    $db = new DB_CONNECT();

    // mysql update row with matched pid acttotaltable(actid, attname, attrole,ans,status)
    $result = mysql_query("UPDATE acttotaltable SET ans = '$ans' WHERE actid = '$actid' AND attname='$attname'");

    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Product successfully updated.";
        
        // echoing JSON response
        echo json_encode($response);
    } else {
        
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
